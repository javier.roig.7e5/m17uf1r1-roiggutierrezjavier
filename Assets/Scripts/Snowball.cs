using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snowball : MonoBehaviour
{
    [SerializeField] private GameObject icePlosion;
    Vector3 snowBallLastPosition;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Fireball"))
        {
            snowBallLastPosition = transform.position;
            Instantiate(icePlosion, snowBallLastPosition, Quaternion.identity);
        }
        Destroy(gameObject);
    }
}
