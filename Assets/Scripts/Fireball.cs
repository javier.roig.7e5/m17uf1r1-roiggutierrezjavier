using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField] private float radio;
    [SerializeField] private float explosionForce;
    private Animator animator;
    Collider2D collider;
    private Rigidbody2D rb;

    void Start()
    {
        animator = GetComponent<Animator>();
        collider = GetComponent<Collider2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        animator.SetBool("explosion", true);
        collider.isTrigger = true;
        rb = GetComponent<Rigidbody2D>();
        rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        Explosion();
    }

    public void Explosion()
    {
        Collider2D[] objetos = Physics2D.OverlapCircleAll(transform.position, radio);

        foreach (Collider2D colisionador in objetos)
        {
            Rigidbody2D rb = colisionador.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                Vector2 direccion = colisionador.transform.position - transform.position;
                float distancia = 1 + direccion.magnitude;
                float fuerzaFinal = explosionForce / distancia;
                rb.AddForce(direccion * fuerzaFinal);
            }
        }

        StartCoroutine(DestroyTime());
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radio);
    }

    private IEnumerator DestroyTime()
    {
        
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}
