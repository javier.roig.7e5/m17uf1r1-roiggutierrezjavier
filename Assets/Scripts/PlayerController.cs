using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D playerRb;
    private float horizontalMove = 0f;
    [SerializeField] private float speed;
    [SerializeField] private float smoothMovement;
    private Vector3 velocity = Vector3.zero;
    private bool lookingRight = true;
    private Animator playerAnimator;

    [SerializeField] private float jumpForce;
    [SerializeField] private LayerMask whatIsGround;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private Vector3 boxDimensions;
    [SerializeField] private bool inGround;
    private bool jump = false;

    public bool BiggerHability = false;
    public bool SmallerHability = false;
    public bool SizeHabilityActive = false;
    public float SizeHabilityTime = 5;

    [SerializeField] private GameObject snowBall;
    [SerializeField] private Transform firePoint;
    [SerializeField] private float projectileSpeed = 50;
    [SerializeField] private bool shootAvailable = true;

    Vector2 lookDirection;
    float lookAngle;



    void Start()
    {
        playerRb = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
    }

    void Update()
    {
        if (SizeHabilityActive)
        {
            SizeHabilityTime -= Time.deltaTime;
            if (SizeHabilityTime <= 0)
            {
                NormalSize();
                SizeHabilityActive = false;
                SizeHabilityTime = 5;
            }
        }
        
        if (!GameManager.Instance.GameStopped)
        {
            horizontalMove = Input.GetAxisRaw("Horizontal") * speed;
            playerAnimator.SetFloat("Horizontal", horizontalMove);

            if (Input.GetButtonDown("Jump"))
            {
                jump = true;
            }
            else if (Input.GetButtonUp("Jump"))
            {
                jump = false;
            }

            lookDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            lookAngle = Mathf.Atan2(lookDirection.y, (lookDirection.x)) * Mathf.Rad2Deg;

            firePoint.rotation = Quaternion.Euler(0, 0, lookAngle);

            if (Input.GetMouseButtonDown(0) && shootAvailable)
            {
                GameObject projectileClone = Instantiate(snowBall);
                projectileClone.transform.position = firePoint.position;
                projectileClone.transform.rotation = Quaternion.Euler(0, 0, lookAngle);

                projectileClone.GetComponent<Rigidbody2D>().velocity = firePoint.right * projectileSpeed;
                StartCoroutine(SnowballCoolDown());
            }
        }
        else
        {
            playerRb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        }
    }

    private void FixedUpdate()
    {
        inGround = Physics2D.OverlapBox(groundCheck.position, boxDimensions, 0f, whatIsGround);
        Move(horizontalMove * Time.deltaTime, jump);
    }

    private void Move(float move, bool jump2)
    {
        Vector3 targetVelocity = new Vector2(move, playerRb.velocity.y);
        playerRb.velocity = Vector3.SmoothDamp(playerRb.velocity, targetVelocity, ref velocity, smoothMovement);

        if (inGround && jump2)
        {
            inGround = false;
            playerRb.AddForce(new Vector2(0f, jumpForce));
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(groundCheck.position, boxDimensions);
    }

    public void SizeHability()
    {
        if (BiggerHability || SmallerHability)
        {
            NormalSize();
        }

        int number = Random.Range(0, 2);
        if (number == 0)
        {
            SizeHabilityActive = true;
            SizeHabilityTime = 5;
            BecomeBigger();
        }
        else if(number == 1)
        {
            SizeHabilityActive = true;
            SizeHabilityTime = 5;
            BecomeSmaller();
        }
    }

    private void BecomeBigger()
    {
        BiggerHability = true;
        speed = speed / 2;
        jumpForce = jumpForce / 2;
        transform.localScale = new Vector2(transform.localScale.x * 2, transform.localScale.y * 2);
    }

    private void NormalSize()
    {
        BiggerHability = false;
        SmallerHability = false;
        speed = 350f;
        jumpForce = 400f;
        transform.localScale = new Vector2(1, 1);
    }

    private void BecomeSmaller()
    {
        SmallerHability = true;
        speed = speed * 2;
        jumpForce = jumpForce * 2;
        transform.localScale = new Vector2(transform.localScale.x / 2, transform.localScale.y / 2);
    }

    private IEnumerator SnowballCoolDown()
    {
        shootAvailable = false;
        yield return new WaitForSeconds(1);
        shootAvailable = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Fireball"))
        {
            playerAnimator.SetBool("dead", true);
            GameManager.Instance.ShowGameOver();
        }else if (collision.gameObject.CompareTag("Poison"))
        {
            SizeHability();
            Destroy(collision.gameObject);
        }
    }
}
