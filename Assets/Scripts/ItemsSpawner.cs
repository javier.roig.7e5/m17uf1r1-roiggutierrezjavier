using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ItemsSpawner : MonoBehaviour
{
    private float minX, maxX;
    [SerializeField] private Transform[] points;
    [SerializeField] private GameObject[] Items;
    [SerializeField] private bool SpawnAvailable;
    [SerializeField] private float TimeBetweenItems;

    void Start()
    {
        maxX = points.Max(point => point.position.x);
        minX = points.Min(point => point.position.x);
        StartCoroutine(WaitTime());
    }

    void Update()
    {
        if (SpawnAvailable && !GameManager.Instance.GameStopped)
        {
            int itemType = Random.Range(1, 11);
            Vector2 randomPosition = new Vector2(Random.Range(minX, maxX), 6.88f);

            if (itemType ==5)
            {
                Instantiate(Items[1], randomPosition, Quaternion.identity);
                GameManager.Instance.fireballsCount++;
            }
            else
            {
                Instantiate(Items[0], randomPosition, Quaternion.identity);
                GameManager.Instance.fireballsCount++;
            }
            StartCoroutine(WaitTime());
        }
    }

    private IEnumerator WaitTime()
    {
        SpawnAvailable = false;
        yield return new WaitForSeconds(TimeBetweenItems);
        SpawnAvailable = true;
    }
}
