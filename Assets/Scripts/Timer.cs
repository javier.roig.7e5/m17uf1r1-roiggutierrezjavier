using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;


public class Timer : MonoBehaviour
{
    public float minut;
    public float second;
    private string minut0, second0;
    public GameObject textoTimer;

    void Start()
    {
        textoTimer = GameObject.Find("TimeText");
    }

    void Update()
    {
        if (minut < 0)
        {
            minut = 0;
            second = 0;
            textoTimer.gameObject.GetComponent<TMP_Text>().text = "00:00";
            if (SceneManager.GetActiveScene().name == "GameMode1")
            {
                GameManager.Instance.ShowWin();
            }
            else if (SceneManager.GetActiveScene().name == "GameMode2")
            {
                GameManager.Instance.ShowGameOver();
            }
        }

        if (!GameManager.Instance.GameStopped)
        {
            second -= Time.deltaTime;
            if (second < 0f)
            {
                minut -= 1;
                second = 59;
            }

            if (minut < 10)
            {
                minut0 = "0";
            }
            else
            {
                minut0 = "";
            }

            if (second < 9.5f)
            {
                second0 = "0";
            }
            else
            {
                second0 = "";
            }
            textoTimer.gameObject.GetComponent<TMP_Text>().text = minut0 + minut.ToString("f0") + ":" + second0 + second.ToString("f0");
        }
    }

}
