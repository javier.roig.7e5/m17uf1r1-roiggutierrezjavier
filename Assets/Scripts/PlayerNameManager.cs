using UnityEngine;
//using UnityEngine.UI;
using TMPro;

public class PlayerNameManager : MonoBehaviour
{
    public TMP_InputField playerNameInput; // Referencia al campo de entrada (Input Field) para el nombre del jugador

    void Start()
    {
        string savedName = PlayerPrefs.GetString("PlayerName", ""); // Obtener el nombre del jugador guardado, si existe
        playerNameInput.text = savedName; // Establecer el texto del campo de entrada con el nombre del jugador guardado
    }

    public void SavePlayerName()
    {
        string playerName = playerNameInput.text; // Obtener el nombre del jugador desde el campo de entrada
        PlayerPrefs.SetString("PlayerName", playerName); // Guardar el nombre del jugador en PlayerPrefs
    }
}
