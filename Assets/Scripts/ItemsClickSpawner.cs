using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ItemsClickSpawner : MonoBehaviour
{
    [SerializeField] private GameObject[] Items;
    [SerializeField] private bool SpawnAvailable;
    [SerializeField] private float TimeBetweenItems;

    void Start()
    {
    }

    void Update()
    {
        if (SpawnAvailable && !GameManager.Instance.GameStopped)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector2 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                clickPosition.y = 6.88f;
                int itemType = Random.Range(1, 11);

                if (itemType == 5)
                {
                    Instantiate(Items[1], clickPosition, Quaternion.identity);
                    GameManager.Instance.fireballsCount++;
                }
                else
                {
                    Instantiate(Items[0], clickPosition, Quaternion.identity);
                    GameManager.Instance.fireballsCount++;
                }
                StartCoroutine(WaitTime());
            }
        }
    }

    private IEnumerator WaitTime()
    {
        SpawnAvailable = false;
        yield return new WaitForSeconds(TimeBetweenItems);
        SpawnAvailable = true;
    }
}
