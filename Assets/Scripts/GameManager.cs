using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    [SerializeField] private GameObject GameWinScreen;
    [SerializeField] private GameObject GameOverScreen;
    public bool GameStopped = false;
    public int fireballsCount = 0;
    public GameObject PlayerNameText;
    public GameObject TotalFireballs;
    private string playerName;

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.Log("There more than 2 GameManagers on the scene!");
        }
    }

    void Start()
    {
        Time.timeScale = 1;
        if (SceneManager.GetActiveScene().name == "GameMode1" || SceneManager.GetActiveScene().name == "GameMode2")
        {
            playerName = PlayerPrefs.GetString("PlayerName");
            GameWinScreen = GameObject.Find("GameWin");
            GameOverScreen = GameObject.Find("GameOver");
            GameWinScreen.SetActive(false);
            GameOverScreen.SetActive(false);
        }
    }

    public void ShowWin()
    {
        GameStopped = true;
        GameWinScreen.SetActive(true);
        PlayerNameText = GameObject.Find("PlayerNameText");
        TotalFireballs = GameObject.Find("TotalFireballsText");
        PlayerNameText.gameObject.GetComponent<TMP_Text>().text = "Name: " + playerName;
        TotalFireballs.gameObject.GetComponent<TMP_Text>().text = "Total Fireballs Spawned: " + fireballsCount;
        StartCoroutine(StopGameTime());
    }

    public void ShowGameOver()
    {
        GameStopped = true;
        GameOverScreen.SetActive(true);
        PlayerNameText = GameObject.Find("PlayerNameText");
        TotalFireballs = GameObject.Find("TotalFireballsText");
        PlayerNameText.gameObject.GetComponent<TMP_Text>().text = "Name: " + playerName;
        TotalFireballs.gameObject.GetComponent<TMP_Text>().text = "Total Fireballs Spawned: " + fireballsCount;
        StartCoroutine(StopGameTime());
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("StartGame");
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadGameMode1()
    {
        SceneManager.LoadScene("GameMode1");
    }

    public void LoadGameMode2()
    {
        SceneManager.LoadScene("GameMode2");
    }

    public void Exit()
    {
        Debug.Log("Salir");
        Application.Quit();
    }

    private IEnumerator StopGameTime()
    {
        yield return new WaitForSeconds(3);
        Time.timeScale = 0;
    }
}
