using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    [SerializeField] private float velocidad;
    [SerializeField] private Transform controladorSuelo;
    [SerializeField] private float distancia;
    [SerializeField] private bool moviendoDerecha;
    private Rigidbody2D rb;
    private Animator playerAnimator;

    public bool BiggerHability = false;
    public bool SmallerHability = false;
    public bool SizeHabilityActive = false;
    public float SizeHabilityTime = 5;

    [SerializeField] private GameObject fireball;
    [SerializeField] private GameObject projectile;
    [SerializeField] private Transform firePoint;
    [SerializeField] private float projectileSpeed = 10;
    [SerializeField] private float _shootCadenceTime;

    float lookAngle;
    private bool _shootAvailable = true;
    private float distance;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (SizeHabilityActive)
        {
            SizeHabilityTime -= Time.deltaTime;
            if (SizeHabilityTime <= 0)
            {
                NormalSize();
                SizeHabilityActive = false;
                SizeHabilityTime = 5;
            }
        }

        if (!GameManager.Instance.GameStopped)
        {
            if (GameObject.FindGameObjectWithTag("Fireball"))
            {
                fireball = GameObject.FindGameObjectWithTag("Fireball");

                distance = Vector2.Distance(transform.position, fireball.transform.position);

                Vector2 lookDirection = fireball.transform.position - transform.position;

                lookAngle = Mathf.Atan2(lookDirection.y, (lookDirection.x)) * Mathf.Rad2Deg;

                firePoint.rotation = Quaternion.Euler(0, 0, lookAngle);


                if (_shootAvailable)
                {
                    StartCoroutine(Cadence());
                    GameObject projectileClone = Instantiate(projectile);
                    projectileClone.transform.position = firePoint.position;
                    projectileClone.transform.rotation = Quaternion.Euler(0, 0, lookAngle);

                    projectileClone.GetComponent<Rigidbody2D>().velocity = firePoint.right * projectileSpeed;
                }
            }
        }
    }

    void FixedUpdate()
    {
        if (!GameManager.Instance.GameStopped)
        {
            RaycastHit2D infomacionSuelo = Physics2D.Raycast(controladorSuelo.position, Vector2.down, distancia);
            rb.velocity = new Vector2(velocidad, rb.velocity.y);
            playerAnimator.SetFloat("Horizontal", rb.velocity.x);
            if (infomacionSuelo == false)
            {
                Girar();
            }
        }
        else
        {
            rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        }
        
    }

    private void Girar()
    {
        moviendoDerecha = !moviendoDerecha;
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
        velocidad *= -1;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(controladorSuelo.transform.position, controladorSuelo.transform.position + Vector3.down * distancia);
    }

    public void SizeHability()
    {
        if (BiggerHability || SmallerHability)
        {
            NormalSize();
        }

        int number = Random.Range(0, 2);
        if (number == 0)
        {
            SizeHabilityActive = true;
            SizeHabilityTime = 5;
            BecomeBigger();
        }
        else if (number == 1)
        {
            SizeHabilityActive = true;
            SizeHabilityTime = 5;
            BecomeSmaller();
        }
    }

    private void NormalSize()
    {
        BiggerHability = false;
        SmallerHability = false;
        transform.localScale = new Vector2(1, 1);
    }
    private void BecomeBigger()
    {
        BiggerHability = true;
        transform.localScale = new Vector2(transform.localScale.x * 2, transform.localScale.y * 2);
    }

    private void BecomeSmaller()
    {
        SmallerHability = true;
        transform.localScale = new Vector2(transform.localScale.x / 2, transform.localScale.y / 2);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Fireball"))
        {
            if (!BiggerHability)
            {
                playerAnimator.SetBool("dead", true);
                GameManager.Instance.ShowWin();
            }
        }
        else if (collision.gameObject.CompareTag("Poison"))
        {
            SizeHability();
            Destroy(collision.gameObject);
        }
    }

    private IEnumerator Cadence()
    {
        _shootAvailable = false;
        yield return new WaitForSeconds(_shootCadenceTime);
        _shootAvailable = true;
    }

}
