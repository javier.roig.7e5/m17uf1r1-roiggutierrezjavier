using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rain : MonoBehaviour
{
    [SerializeField] private GameObject Mushroom;
    Vector3 MushroomSpawnLocation;
    void Start()
    {
        MushroomSpawnLocation = transform.position;
        MushroomSpawnLocation.y = -4.168827f;
        StartCoroutine(RainDuration());
    }

    private IEnumerator RainDuration()
    {
        yield return new WaitForSeconds(2);
        Instantiate(Mushroom, MushroomSpawnLocation, Quaternion.identity);
        Destroy(gameObject);
    }
}
