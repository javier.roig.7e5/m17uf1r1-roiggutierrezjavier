using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Iceplosion : MonoBehaviour
{
    [SerializeField] private GameObject rain;
    Vector3 RainSpawnLocation;
    void Start()
    {
        RainSpawnLocation = transform.position;
        RainSpawnLocation.y = 5.05f;
        StartCoroutine(RainWaitTime());
    }

    private IEnumerator RainWaitTime()
    {
        yield return new WaitForSeconds(1);
        Instantiate(rain, RainSpawnLocation, Quaternion.identity);
        Destroy(gameObject);
    }
}
